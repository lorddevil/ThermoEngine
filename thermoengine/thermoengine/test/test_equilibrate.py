from __future__ import annotations # Enable Python 4 type hints in Python 3

from pytest import raises

import numpy as np

# API maintained for equilibrate package
from thermoengine.equilibrate import PhaseLibrary, GibbsMinimizer, System

import thermoengine as thermo
from thermoengine import samples as smp



from thermoengine.const import units
from thermoengine.core import UnorderedList

from thermoengine.test_utils import are_close, are_roughly_close, dict_values_are_close

CR_SPINEL_CATION_MIN = 0.1

DEFAULT_RESULT = True

bermanDB = thermo.model.Database(database='Berman')
stixrudeDB = thermo.model.Database(database='Stixrude')
MgO_SiO2_phase_library = PhaseLibrary(stixrudeDB.get_phases(
    ['Per',  # MgO
     'Fo', 'MgWds', 'MgRwd',  # Mg2SiO4
     'En', 'cEn', 'hpcEn', 'Maj', 'MgAki', 'MgPrv', 'MgPpv',  # MgSiO3
     'Qz', 'Coe', 'Sti', 'Seif']  # SiO2
))
all_stixrude_phases = stixrudeDB.get_all_phases()
stixrude_phs_library = PhaseLibrary(all_stixrude_phases)

class TestEquilUtils:
    def test_should_define_unordered_list(self):
        assert UnorderedList(['Ol', 'MgWds', 'MgRwd']) == ['Ol', 'MgWds', 'MgRwd']
        assert UnorderedList(['Ol', 'MgWds', 'MgRwd']) == ['Ol', 'MgRwd', 'MgWds']
        assert not UnorderedList(['Ol', 'Ol', 'MgRwd']) == ['Ol', 'MgRwd']
        assert UnorderedList(['Ol', 'Ol', 'MgRwd']) == ['Ol', 'MgRwd', 'Ol']


class TestSystemEquilibrate:
    BSE_comp = thermo.OxideComp(SiO2=38.80, Al2O3=1.79, FeO=5.75, MgO=50.83,
                                CaO=2.58, Na2O=0.25)
    def test_should_store_and_compare_environmental_conditions(self):
        system = thermo.equilibrate.System(T=1000.0, P=1)
        assert system == thermo.equilibrate.System(T=1000, P=1)
        assert not system == thermo.equilibrate.System(T=0, P=0)

    def test_should_store_and_compare_composition(self):
        system = thermo.equilibrate.System(
            comp=thermo.OxideComp(MgO=0.5, SiO2=0.5))

        assert system == thermo.equilibrate.System(
            comp=thermo.OxideComp(MgO=0.5, SiO2=0.5))
        assert not system == thermo.equilibrate.System(
            comp=thermo.OxideComp(MgO=0.1, SiO2=0.9))
        assert not system == thermo.equilibrate.System(
            comp=thermo.OxideComp(Al2O3=1))

    def test_should_define_phase_library(self):
        phs_lib = PhaseLibrary(
            stixrudeDB.get_phases(['Fo', 'MgRwd', 'MgWds']))

        assert phs_lib.available_phase_abbrevs == ['Fo', 'MgRwd', 'MgWds']
        assert phs_lib.available_phase_abbrevs == ['MgRwd', 'Fo', 'MgWds']
        assert not phs_lib.available_phase_abbrevs == ['MgRwd', 'Fo']

    def test_should_find_stable_mantle_olivine_polymorphs(self):
        phs_lib = PhaseLibrary(
            stixrudeDB.get_phases(['Fo', 'MgRwd', 'MgWds']))
        system = thermo.equilibrate.System(
            T=1673, comp=thermo.OxideComp(MgO=2, SiO2=1), phase_library=phs_lib)
        assert system.update(P=10*units.GPA).stable_assemblage == ['Fo']
        assert system.update(P=15*units.GPA).stable_assemblage == ['MgWds']
        assert system.update(P=20*units.GPA).stable_assemblage == ['MgRwd']
        assert not system.stable_assemblage == ['Fo']

    def test_should_reveal_miscibility_gap_for_olivine_polymorph(self):
        Wds_only = PhaseLibrary([stixrudeDB.get_phase('Wds')])
        MgFeSiO4 = thermo.OxideComp(MgO=1, FeO=1, SiO2=1)
        system = System(comp=MgFeSiO4, phase_library=Wds_only)
        T_crit = 496.1
        dT = 5
        assert not system.update(T=1500).spans_miscibility_gap
        assert not system.update(T=T_crit+dT).spans_miscibility_gap
        assert system.update(T=T_crit-dT).spans_miscibility_gap

    def test_should_locate_critical_pt_for_olivine_polymorph_at_desired_resolution(self):
        phs_lib = PhaseLibrary([stixrudeDB.get_phase('Wds')])
        MgFeSiO4 = thermo.OxideComp(MgO=1, FeO=1, SiO2=1)
        T_crit = 496.1

        system = System(T=T_crit-1, comp=MgFeSiO4, phase_library=phs_lib)

        system_hires = System(T=T_crit - 1, comp=MgFeSiO4, phase_library=phs_lib,
                              options={'grid_spacing':0.01})

        assert not system.spans_miscibility_gap
        assert system_hires.spans_miscibility_gap

    def test_should_reveal_miscibility_gap_for_ternary_feldspar(self):
        Fsp_only = PhaseLibrary([bermanDB.get_phase('Fsp')])
        Ab = thermo.ElemComp(K=0, Na=1, Ca=0, Al=1, Si=3, O=8)
        An = thermo.ElemComp(K=0, Na=0, Ca=1, Al=2, Si=2, O=8)
        Or = thermo.ElemComp(K=1, Na=0, Ca=0, Al=1, Si=3, O=8)
        system = System(T=600, phase_library=Fsp_only)

        endmem = Ab
        stable_binary = Ab+An
        unstable_binary = An+Or
        unstable_ternary = Ab+An+Or

        assert not system.update(comp=endmem).spans_miscibility_gap
        assert not system.update(comp=stable_binary).spans_miscibility_gap
        assert system.update(comp=unstable_binary).spans_miscibility_gap
        assert system.update(comp=unstable_ternary).spans_miscibility_gap

    def test_should_resolve_exsolved_samples_without_grid_aliasing_for_ternary_feldspar(self):
        Fsp_only = PhaseLibrary([bermanDB.get_phase('Fsp')])
        Ab = thermo.ElemComp(K=0, Na=1, Ca=0, Al=1, Si=3, O=8)
        An = thermo.ElemComp(K=0, Na=0, Ca=1, Al=2, Si=2, O=8)
        Or = thermo.ElemComp(K=1, Na=0, Ca=0, Al=1, Si=3, O=8)
        system = System(T=600, phase_library=Fsp_only, options={'grid_spacing': 1/10})

        stable_binary = Ab + An
        unstable_binary = An+Or
        unstable_ternary = Ab+An+Or

        assert system.update(comp=stable_binary).resolves_any_exsolved_samples()
        assert system.update(comp=unstable_binary).resolves_any_exsolved_samples()
        assert system.update(comp=unstable_ternary).resolves_any_exsolved_samples()

    def test_should_resolve_single_phase_without_grid_aliasing_for_ternary_garnet(self):
        Grt_only = PhaseLibrary([bermanDB.get_phase('Grt')])
        Alm = thermo.ElemComp(Fe=3, Mg=0, Ca=0, Al=2, Si=3, O=12)
        Grs = thermo.ElemComp(Fe=0, Mg=0, Ca=3, Al=2, Si=3, O=12)
        Prp = thermo.ElemComp(Fe=0, Mg=3, Ca=0, Al=2, Si=3, O=12)

        system = System(T=600, phase_library=Grt_only, options={'grid_spacing': 1/4})

        stable_ternary = Alm+Grs+Prp
        system.update(comp=stable_ternary)
        assert system.update(comp=stable_ternary).resolves_any_exsolved_samples()


    def test_should_distinguish_miscibility_gap_from_multiple_pure_phases(self):
        Mg2SiO4, SiO2 = thermo.OxideComp(MgO=2, SiO2=1), thermo.OxideComp(SiO2=1)
        system = System(T=500, phase_library=MgO_SiO2_phase_library)
        pure_phases = Mg2SiO4 + SiO2
        assert not system.update(comp=pure_phases).spans_miscibility_gap

    def xtest_should_find_olivine_stable_for_all_Fe_contents_at_1_bar(self):
        """
        data source: Stixrude & Lithgow-Bertelloni (2011), Fig. 10
        """
        # TODO: replace list-like composition with OxideComp
        phs_lib = PhaseLibrary(stixrudeDB.get_phases(
            ['Ol', 'Rwd', 'Wds']))
        Mg2SiO4 = thermo.OxideComp(MgO=2, SiO2=1)
        Fe2SiO4 = thermo.OxideComp(FeO=2, SiO2=1)
        endmems = np.array([Mg2SiO4,Fe2SiO4])

        system = thermo.equilibrate.System(
            T=1673, P=1, comp=endmems.dot([0.5,0.5]), phase_library=phs_lib)
        comp = endmems.dot([0.99, 0.01])
        system.update(comp=comp)

        assert system.update(comp=endmems.dot([0.99, 0.01])).stable_assemblage == ['Ol']
        assert system.update(comp=endmems.dot([0.5, 0.5])).stable_assemblage == ['Ol']
        assert system.update(comp=endmems.dot([0.01, 0.99])).stable_assemblage == ['Ol']

    def test_should_select_equilibration_method(self):
        system = thermo.equilibrate.System(equil_method=GibbsMinimizer.GRIDDED)
        assert system.equil_method == GibbsMinimizer.GRIDDED
        assert not system.equil_method == GibbsMinimizer.NONLINEAR

        system = thermo.equilibrate.System(equil_method=GibbsMinimizer.NONLINEAR)
        assert system.equil_method == GibbsMinimizer.NONLINEAR

    def xtest_should_find_PX_dependent_stable_solid_solution_phase(self):
        """
        data source: Stixrude & Lithgow-Bertelloni (2011), Fig. 10
        """
        # TODO: replace list-like composition with OxideComp
        phs_lib = PhaseLibrary(stixrudeDB.get_phases(
            ['Ol', 'Rwd', 'Wds']))
        system = thermo.equilibrate.System(T=1673, P=1, comp=[0.5, 0.5],
                                           phase_library=phs_lib)
        # Mg-rich olivine polymorphs
        assert system.update(P=10*units.GPA, comp=[0.99, 0.01]).stable_assemblage == ['Ol']
        assert system.update(P=15*units.GPA, comp=[0.99, 0.01]).stable_assemblage == ['Wds']
        assert system.update(P=20*units.GPA, comp=[0.99, 0.01]).stable_assemblage == ['Rwd']

        # Mg/Fe mixed olivine polymorphs
        assert system.update(P=8*units.GPA, comp=[0.5, 0.5]).stable_assemblage == ['Ol']
        assert system.update(P=12*units.GPA, comp=[0.5, 0.5]).stable_assemblage == ['Rwd']

        # Fe-rich olivine polymorphs
        assert system.update(P=5*units.GPA, comp=[0.01, 0.99]).stable_assemblage == ['Ol']
        assert system.update(P=7*units.GPA, comp=[0.01, 0.99]).stable_assemblage == ['Rwd']

    def test_should_find_mantle_assemblages(self):
        # TODO NEEDS SPEEDUP
        system = System(T=1600, comp=self.BSE_comp,
                        options={'grid_spacing':1/10},
                        phase_library=stixrude_phs_library)


        assert system.update(P=5 * units.GPA).stable_assemblage == UnorderedList(
            ['Ol', 'Opx', 'Cpx', 'Grt'])
        # assert system.update(P=10 * units.GPA).stable_assemblage == UnorderedList(
        #     ['Ol', 'hpCpx', 'Cpx', 'Grt'])
        assert system.update(P=17 * units.GPA).stable_assemblage == UnorderedList(
            ['Wds', 'Grt'])
        # assert system.update(P=24 * units.GPA).stable_assemblage == UnorderedList(
        #     ['CaPrv', 'PrvS', 'Fp', 'Grt'])
        assert system.update(P=100 * units.GPA).stable_assemblage == UnorderedList(
            ['PrvS', 'CaPrv', 'Fp', 'CfS'])

        assert system.update(P=136 * units.GPA).stable_assemblage == UnorderedList(
            ['PpvS', 'CaPrv', 'Fp', 'CfS'])


class TestGibbsMinimizer:
    def test_should_set_sample_library_to_near_endmember_comps_if_nonlinear_method(self):
        PrvS, Ol, Qz = stixrudeDB.get_phases(['PrvS', 'Ol', 'Qz'])

        phs_lib = PhaseLibrary([PrvS])
        minimizer = GibbsMinimizer(method=GibbsMinimizer.NONLINEAR,
                                   phase_library=phs_lib)
        self._assert_tests_each_endmember(minimizer.sample_library, endmem_num=3)  #

        phs_lib = PhaseLibrary([PrvS, Ol, Qz])
        minimizer = GibbsMinimizer(method=GibbsMinimizer.NONLINEAR,
                                   phase_library=phs_lib)
        endmem_total = 3 + 2 + 1  # (MgSi, FeSi, AlAl)O3 + (Mg, Fe)2SiO4 + SiO2
        assert len(minimizer.sample_library) == endmem_total

    def _assert_tests_each_endmember(self, sample_library, endmem_num=1):
        for samp, iX_endmem in zip(sample_library, np.eye(endmem_num)):
            assert are_close(samp.X, iX_endmem, abs_tol=1e-5)

    def test_should_retrieve_stable_assemblage_after_equilibration(self):
        minimizer = GibbsMinimizer(
            phase_library=MgO_SiO2_phase_library)
        assert minimizer.stable_assemblage is None
        assert minimizer.equilibrate(
            T=1000,P=1,comp=thermo.OxideComp(SiO2=1, MgO=2)).stable_assemblage is not None
        assert minimizer.stable_assemblage == minimizer.current_assemblage

    def test_should_equilibrate_by_chemical_exchange_on_stable_binary_solution_sample(self):
        phs = stixrudeDB.get_phase('Ol')
        sample = smp.SampleMaker.get_sample(phs, X=[0.5, 0.5], T=1673, P=1)

        samp_equil = sample.equilibrate_by_chemical_exchange(
            sample.chem_potential(X=[0.99, 0.01]))
        assert are_close(samp_equil.X, [0.99, 0.01], abs_tol=1e-4)

        samp_equil = sample.equilibrate_by_chemical_exchange(
            sample.chem_potential(X=[0.3, 0.7]))
        assert are_close(samp_equil.X, [0.3, 0.7], abs_tol=1e-4)

    def test_should_raise_missing_phase_library(self):
        with raises(GibbsMinimizer.MissingPhaseLibraryError):
            GibbsMinimizer()


    def test_should_raise_selected_phases_empty_error(self):
        minimizer = GibbsMinimizer(phase_library=PhaseLibrary(
            stixrudeDB.get_phases(['Per', 'Fo', 'En', 'Qz'])))

        with raises(minimizer.SelectedPhasesEmptyError):
            assert minimizer.find_stable_phase(
                T=500, P=1, comp=thermo.OxideComp(Al2O3=1))

    def test_should_filter_phases_by_composition(self):
        phs_lib = PhaseLibrary(stixrudeDB.get_phases(
            ['Per',  # MgO
             'Fo', 'MgWds', 'MgRwd',  # Mg2SiO4
             'En', 'cEn', 'hpcEn', 'Maj', 'MgAki', 'MgPrv', 'MgPpv',  # MgSiO3
             'Qz', 'Coe', 'Sti', 'Seif']  # SiO2
        ))
        minimizer = GibbsMinimizer(method=GibbsMinimizer.GRIDDED,
                                   phase_library=phs_lib)
        SiO2 = thermo.OxideComp(SiO2=1)

        minimizer.init_current_assemblage(T=300, P=1, comp=SiO2)
        assert len(minimizer.current_assemblage)==len(phs_lib.available_phases)
        assert len(minimizer.filter_phases_by_comp(SiO2)) == 4


    def test_should_get_valid_minimizer_methods(self):
        methods = UnorderedList(GibbsMinimizer.get_valid_methods())
        assert methods == ['GibbsMinimizer.GRIDDED', 'GibbsMinimizer.NONLINEAR']

    def test_should_raise_invalid_gibbs_minimizer_type(self):
        with raises(GibbsMinimizer.InvalidMinimizerMethod):
            GibbsMinimizer(method='gridded', phase_library=MgO_SiO2_phase_library)

    def test_should_find_single_stable_pure_phase_with_desired_composition(self):
        phs_lib = PhaseLibrary(stixrudeDB.get_phases(
            ['Per',  # MgO
             'Fo', 'MgWds', 'MgRwd',  # Mg2SiO4
             'En', 'cEn', 'hpcEn', 'Maj', 'MgAki', 'MgPrv', 'MgPpv',  # MgSiO3
             'Qz', 'Coe', 'Sti', 'Seif']  # SiO2
        ))
        minimizer = GibbsMinimizer(method=GibbsMinimizer.GRIDDED,
                                   phase_library=phs_lib)

        assert minimizer.find_stable_phase(
            T=500, P=1, comp=thermo.OxideComp(MgO=2, SiO2=1)).name == 'Fo'
        assert minimizer.find_stable_phase(
            T=500, P=1, comp=thermo.OxideComp(SiO2=1)).name == 'Qz'
        assert minimizer.find_stable_phase(
            T=500, P=1, comp=thermo.OxideComp(MgO=1, SiO2=1)).name == 'En'
        assert minimizer.find_stable_phase(
            T=500, P=75*units.GPA,
            comp=thermo.OxideComp(MgO=1, SiO2=1)).name == 'MgPrv'

    def test_should_find_pure_phase_assemblage_with_desired_composition(self):
        minimizer = GibbsMinimizer(method=GibbsMinimizer.GRIDDED,
                                   phase_library=MgO_SiO2_phase_library,
                                   )
        comp_Mg2SiO4, comp_SiO2 = thermo.OxideComp(MgO=2, SiO2=1), thermo.OxideComp(SiO2=1)

        minimizer.equilibrate(T=500, P=1, comp=comp_Mg2SiO4 + comp_SiO2)
        assert UnorderedList([samp.name for samp in minimizer.current_assemblage]) == ['Fo', 'Qz']

        minimizer.equilibrate(T=500, P=75*units.GPA, comp=comp_Mg2SiO4)
        assert UnorderedList([samp.name for samp in minimizer.current_assemblage]) == ['MgPrv', 'Per']

    def test_should_find_binary_miscibility_gap_using_grid_of_fixed_compositions(self):
        phs = stixrudeDB.get_phase('Wds')
        minimizer = GibbsMinimizer(method=GibbsMinimizer.GRIDDED,
                                   phase_library=PhaseLibrary([phs]),
                                   grid_spacing=0.02)
        MgFeSiO4 = thermo.OxideComp(MgO=1, FeO=1, SiO2=1)

        assert len(minimizer.equilibrate(T=1500, P=1, comp=MgFeSiO4).current_assemblage) == 1
        assert len(minimizer.equilibrate(T=500, P=1, comp=MgFeSiO4).current_assemblage) == 1
        assert len(minimizer.equilibrate(T=490, P=1, comp=MgFeSiO4).current_assemblage) == 2

    def test_should_find_ternary_miscibility_gap_using_grid_of_fixed_comps(self):

        Fsp = bermanDB.get_phase('Fsp')
        Ab = thermo.ElemComp(K=0, Na=1, Ca=0, Al=1, Si=3, O=8)
        An = thermo.ElemComp(K=0, Na=0, Ca=1, Al=2, Si=2, O=8)
        Or = thermo.ElemComp(K=1, Na=0, Ca=0, Al=1, Si=3, O=8)
        comp = (Ab + An + Or)*(1/3)
        minimizer = GibbsMinimizer(
            method=GibbsMinimizer.GRIDDED,
            phase_library=PhaseLibrary(phase_models=[Fsp]),
            grid_spacing=0.1
        )


        assert len(minimizer.equilibrate(T=300, P=1, comp=comp).current_assemblage)==3
        [samp.X for samp in minimizer.current_assemblage]

    # def test_should_remove_gridding_artifact(self):
    #     Wds = stixrudeDB.get_phase('Wds')
    #     Sti = stixrudeDB.get_phase('Sti')
    #     Sti_sample = smp.SampleMaker.get_sample(Sti)
    #     Wds_samples = [smp.SampleMaker.get_sample(Wds, X=[1,0]),
    #                    smp.SampleMaker.get_sample(Wds, X=[0,1])]
    #     assem = smp.Assemblage(Wds_samples+[Sti_sample])
    #
    #     assert assem.get_subset_by_phase('Wds') == smp.Assemblage(Wds_samples)
    #     assert assem.get_subset_by_phase('Sti') == smp.Assemblage([Sti_sample])
    #
    #     assem = minimizer.equilibrate().stable_assemblage
    #     assert len(assem.get_subset_by_phase('Wds')) ==

class PhaseAccuracy:


    def xtest_should_show_Wds_critical_pt_at_1000K(self):
        # store info on macroscopic vs microscopic W exchange energy
        pass


